/*
 * Copyright (c) 2019, Centre for Informatics and 
 * Systems of the University of Coimbra.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Powertrace and KIBAM battery
 * \author
 *         Code by Marco Silva <mass@dei.uc.pt>
 */



#include "contiki.h"
#include "lib/list.h"

#include "net/rime/rime.h"
#include <stdio.h>

#include "powertrace.h"       /* Battery Model */
#include "sensor-node.h"
#include "neighbor.h"

/*---------------------------------------------------------------------------*/
/* This function is called whenever a broadcast message is received. */
static uint8_t seqno;
static void
broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
  struct neighbor *n;
  struct broadcast_message *m;
  uint8_t seqno_gap;

  /* The packetbuf_dataptr() returns a pointer to the first data byte
     in the received packet. */
  m = packetbuf_dataptr();

  /* Check if we already know this neighbor. */
  for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {

    /* We break out of the loop if the address of the neighbor matches
       the address of the neighbor from which we received this
       broadcast message. */
    if(linkaddr_cmp(&n->addr, from)) {
      break;
    }
  }

  /* If n is NULL, this neighbor was not found in our list, and we
     allocate a new struct neighbor from the neighbors_memb memory
     pool. */
  if(n == NULL) {
    n = memb_alloc(&neighbors_memb);

    /* If we could not allocate a new neighbor entry, we give up. We
       could have reused an old neighbor entry, but we do not do this
       for now. */
    if(n == NULL) {
      return;
    }

    /* Initialize the fields. */
    linkaddr_copy(&n->addr, from);
    n->last_seqno = m->seqno - 1;
    n->avg_seqno_gap = SEQNO_EWMA_UNITY;
    
    /* Place the neighbor on the neighbor list. */
    list_add(neighbors_list, n);
  }

  /* We can now fill in the fields in our neighbor entry. */
  n->last_rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);
  n->last_lqi = packetbuf_attr(PACKETBUF_ATTR_LINK_QUALITY);
  n->batt = m->batt;
  n->hops = m->hops;
  

  /* Compute the average sequence number gap we have seen from this neighbor. */
  seqno_gap = m->seqno - n->last_seqno;
  n->avg_seqno_gap = (((uint32_t)seqno_gap * SEQNO_EWMA_UNITY) *
                      SEQNO_EWMA_ALPHA) / SEQNO_EWMA_UNITY +
                      ((uint32_t)n->avg_seqno_gap * (SEQNO_EWMA_UNITY -
                                                     SEQNO_EWMA_ALPHA)) / SEQNO_EWMA_UNITY;

  /* Remember last seqno we heard. */
  n->last_seqno = m->seqno;
}

/* This is where we define what function to be called when a broadcast
   is received. We pass a pointer to this structure in the
   broadcast_open() call below. */
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};

/*---------------------------------------------------------------------------*/
void print_neighbors(){
  struct neighbor *n;
  int num = 1;
  int i1, i2;
  printf("Neighbor List:\n");
  for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
    i1 = (int) n->batt;
    i2 = ((n->batt) - i1) * 100;
    printf("%d. Address Number: %d.%d | Bat level: %d.%d | Sink cost %d (hops) \n",num, (int)(n->addr.u8[0]),(int)(n->addr.u8[1]), 
          i1,i2,(int)n->hops);
    num++;
  }
}

/*---------------------------------------------------------------------------*/
/* sink should be unique */
int get_sink_cost(){
  struct neighbor *n;
  int cost = 0;
  int aux = -1;
  for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
      if(linkaddr_cmp(&n->addr, &sink_node_addr)){
        cost = 1;
        return cost;      
      }else{
        cost = n->hops;
        if(cost > aux && aux != -1){
          cost = aux;
        }
        aux = cost;
      }
  }
  return cost + 1 ;
} 
/*---------------------------------------------------------------------------*/
void refresh_broadcast_msg(){
  struct broadcast_message msg;
  msg.seqno = seqno;
  msg.batt = get_battery_charge();
  msg.hops = get_sink_cost();
  packetbuf_copyfrom(&msg, sizeof(struct broadcast_message));
  broadcast_send(&broadcast);
  seqno++;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data)
{
  static struct etimer et;
  
  struct broadcast_message msg;
  static struct etimer intervaltimer;
  static clock_time_t interval;
  
  PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

  PROCESS_BEGIN();

  broadcast_open(&broadcast, 129, &broadcast_call);
  printf("[Sensor Node] Broadcast Start\n");
  interval = MIN_INTERVAL;
  while(1) {

    etimer_set(&intervaltimer, interval);
  
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    msg.seqno = seqno;
    msg.batt = get_battery_charge();
    msg.hops = get_sink_cost();
    packetbuf_copyfrom(&msg, sizeof(struct broadcast_message));
    broadcast_send(&broadcast);
    seqno++;
    PROCESS_WAIT_UNTIL(etimer_expired(&intervaltimer));
    interval *= 1.02;
    if(interval >= (MAX_INTERVAL)) {
      interval = (MAX_INTERVAL);
    }
      
  }

  PROCESS_END();
}