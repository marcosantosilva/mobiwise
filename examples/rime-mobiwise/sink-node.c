/*
 * Copyright (c) 2019, Centre for Informatics and 
 * Systems of the University of Coimbra.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Powertrace and KIBAM battery
 * \author
 *         Code by Marco Silva <mass@dei.uc.pt>
 */

#include "contiki.h"            /* Contiki OS */
#include "net/rime/rime.h"      /* Rime Network Stack Protocol */
#include <stdio.h>
#include <string.h>

#include "neighbor.c"           /* Neighbord List*/

#define DEBUG 1
#if DEBUG
#define DBG(...) printf(__VA_ARGS__);
#else
#define DBG(...)
#endif

static struct mesh_conn mesh;
int all_payloads = 0;
/*---------------------------------------------------------------------------*/
PROCESS(sink_node_process, "Sink Node Process");
PROCESS(broadcast_process, "Broadcast process");
AUTOSTART_PROCESSES(&sink_node_process, &broadcast_process);
/*---------------------------------------------------------------------------*/
/* Callback to recv packets */
static void
recv(struct mesh_conn *c, const linkaddr_t *from)
{
  int payload_counter = 0;
  int j = 0;
  char find[1] = "H";
  char string[PAYLOAD_MAX_LEN];
  
  sprintf(string,"%s", (char *)packetbuf_dataptr());

  // Increment hops counter and count numbers of payloads
  for (j = 0; j < strlen(string) +1; j++)
  {
    if(find[0] == string[j]){
      payload_counter++;
    }
  }

  all_payloads = all_payloads + payload_counter;
  
  linkaddr_t sender;
  #if TIMESYNCH_CONF_ENABLED
     unsigned int recv_time = timesynch_time();
  #else    
      unsigned int recv_time = clock_time();
  #endif
 
  linkaddr_copy(&sender, packetbuf_addr(PACKETBUF_ADDR_ESENDER));
  printf("[Sink Node] Data received from %d.%d | %.*s (%d)\n",
	 from->u8[0], from->u8[1],
  packetbuf_datalen(), (char *)packetbuf_dataptr(), packetbuf_datalen());
  printf("[Sink Node] Payloads received: %d from %d.%d\n", payload_counter,  from->u8[0], from->u8[1]);
  printf("[Sink Node] Received time: %u\n", recv_time);
  printf("[Sink Node] Payloads received all sumulation: %d\n",all_payloads);

}

const static struct mesh_callbacks callbacks = {recv};
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sink_node_process, ev, data)
{
  #if TIMESYNCH_CONF_ENABLED
  timesynch_set_authority_level(0);
  #endif
  PROCESS_EXITHANDLER(mesh_close(&mesh);)

  PROCESS_BEGIN();    
  DBG("[Sink Node] Process begin.\n");
  /* Wait two seconds before starting */
  mesh_open(&mesh, 132, &callbacks);
  DBG("[Sink Node] Opened Mesh Connection.\n");

  while(1) {
	  PROCESS_YIELD();

  }
  DBG("[Sink Node] Process End.\n");
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/