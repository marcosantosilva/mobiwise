/*
 * Copyright (c) 2019, Centre for Informatics and 
 * Systems of the University of Coimbra.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Powertrace and KIBAM battery
 * \author
 *         Code by Marco Silva <mass@dei.uc.pt>
 */

#include "contiki.h"                   /* Contiki OS */
#include "dev/serial-line.h"           /* For Serial Input Commands */
#include "dev/sht11/sht11-sensor.h"    /* SHT11 Temperature and Humidity Sensor */
#include "net/rime/packetqueue.h"      /* Packet Queue Management */
#include "dev/battery-sensor.h"        /* Battery Sensor */
#include "lib/random.h"                /* Random Number Generation */
#include "lib/memb.h"                  /* Dynamic Memory */

#include "neighbor.c"                  /* Neighbord List*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define DEBUG 1
#if DEBUG
#define DBG(...) printf(__VA_ARGS__);
#else
#define DBG(...)
#endif

static struct mesh_conn  mesh;

int simu_timeslot = 1;
const int event_duration = 4; // timeslots

const int event_timeslot[42] = {8,	10,	11,	13,	15,	17,	19,	20,	22,	25,	27,	28,	30,	32,	34,	37,	40,	45,	48,	50,	53,	65,	69,	70,	71,	74,
76,	77,	78,	83,	84,	87,	90,	92,	94,	96,	98,	103,	106,	109,	110,	113};

const int event_capture[42][4] = {
{9,	  10,	0,	0},
{101,	113,	112,	0},
{112,	0,	0,	0},
{77,	78,	88,	0},
{82,	83,	93,	0},
{73,	82,	83,	0},
{34,	44,	45,	0},
{71,	81,	115,	0},
{14,	15,	24,	0},
{80,	81,	90,	91},
{4,	  0,	0,	0},
{120,	119,	0,	0},
{5,	  6,	0,	0},
{3,	  4,	13,	0},
{84,	94,	95,	0},
{78,	79,	88,	89},
{27,	36,	37,	0},
{105,	106,	0,	0},
{11,	0,	0,	0},
{48,	58,	0,	0},
{37,	38,	47,	48},
{114,	113,	0,	0},
{84,	85,	94,	95},
{20,	21,	30,	0},
{50,	60,	61,	0},
{45,	46,	55,	56},
{68,	77,	78,	0},
{72,	82,	0,	0},
{82,	0,	0,	0},
{27,	28,	37,	38},
{80,	81,	90,	0},
{116,	115,	0,	0},
{12,	13,	0,	0},
{7,	  8,	18,	0},
{56,	47,	0,	0},
{3,	  4,	13,	0},
{35,	36,	45,	0},
{62,	63,	73,	0},
{20,	21,	30,	31},
{84,	85,	0,	0},
{27,	28,	37,	0},
{52,	53,	63,	0}};

/* Define a packet queue for sending */
PACKETQUEUE(sensor_packet_queue, MAX_QUEUEBUF);

//Aux to memory to packet queue
MEMB(record_memb, struct record, 11*MAX_QUEUEBUF);
LIST(record_list);

//Memory to store packets on the packet queue
MEMB(packet_memb, struct packetqueue_member, MAX_QUEUEBUF+1);
LIST(packet_list);

struct packetqueue_member *packet;
/*---------------------------------------------------------------------------*/
PROCESS(sensor_node_process, "Sensor Node Process");
PROCESS(serial_input, "Serial Commands Process");
PROCESS(broadcast_process, "Broadcast process");  
AUTOSTART_PROCESSES(&sensor_node_process, &serial_input, &broadcast_process);
/*---------------------------------------------------------------------------*/
// Count number of payloads in the entire packets queue
static int payload_queue_number(void);
// Display on the screen all the packets in the queue
static void packet_queue_show();
// Get number of packets in queue
int packet_queue_number();
// Remove first packet in queue
static void packet_queue_remove();
// Allocate new packet
// if packet queue is full, discard packet more old
static void packet_queue_allocate(char *payload);
// Get number of packets to transmit
static int packets_to_transmit();
// Get number of payloads to trasnsmit
static int payloads_to_transmit();
//---------------------------------------------------------------------------/
/*---------------------------------------------------------------------------*/
// mesh callback triggered when a packet is succesfully sent by the multihop routines /
static void
sent(struct mesh_conn *c){
  printf("Packet sent!. Cost: %d\n", get_sink_cost());
}
/*---------------------------------------------------------------------------*/
/* Count payloads in given packet */
static int
count_payloads(char* string){
  int counter = 0;
  int j = 0;
  char find[1] = "H";
  for (j = 0; j < strlen(string) +1; j++)
  {
    if(find[0] == string[j]){
      counter++;
    }
  }
  return counter;
}
/*---------------------------------------------------------------------------*/
static int
is_event(char* string){
  int j = 0;
  char find[1] = "E";
  for (j = 0; j < strlen(string) +1; j++)
  {
    if(find[0] == string[j]){
      return 1;
    }
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
static void
send_one_packet(char *payload, linkaddr_t *to){

  int payload_counter = count_payloads(payload);
  printf("Sending mensage to %d.%d | Cost: %d Hops\n", to->u8[0],to->u8[1], get_sink_cost());

  int j = 0;
  char find[1] = "H";
  char final[PAYLOAD_MAX_LEN+2];
  sprintf(final,"%s",payload);

  // Increment hops counter and count numbers of payloads
  for (j = 0; j < strlen(final) +1; j++)
  {
      if(find[0] == final[j]){
        final[j+2] = final[j+2] +1;  //H:1
      }
  }

  packetbuf_copyfrom(final, strlen(final)+1);
  
  if(mesh_send(&mesh, to)){
    printf("Payloads number to send: %d (%d)\n", payload_counter, strlen(final));
    sensor_doing_operation(2);
    if (AGGREGATION){
      add_comunications(1,payload_counter,0,0);
    }else{
      add_comunications(payload_counter,payload_counter,0,0);
    }
  }else{
    printf("Message NOT sent.\n");
    printf("ERROR: PAYLOADS DISCARD: %d. Cost: %d (%d)\n", payload_counter, get_sink_cost(), strlen(final));
    return;
  }
}
/*---------------------------------------------------------------------------*/
static void
send_all_packets(linkaddr_t *to){

  int i = 0;
  int j = 0;
  int queue_length = packet_queue_number();
  char find[1] = "H";
  
  printf("Sending %d packets\n", queue_length);
  printf("Sending mensage to %d.%d | Cost: %d Hops\n", to->u8[0],to->u8[1], get_sink_cost()); 
  sensor_doing_operation(2);
  for (i = 0; i < queue_length; ++i) {
    /* We should send the first packet from the queue. */
    packet = list_head(packet_list);

    // Increment hops counter and count numbers of payloads
    for (j = 0; j < strlen(packet->payload) +1; j++)
    {
      if(find[0] == packet->payload[j]){
        packet->payload[j+2] = packet->payload[j+2] +1;  //H:1
      }
    }
    packetbuf_copyfrom(packet->payload, strlen(packet->payload)+1);
    if (mesh_send(&mesh, to)){
      printf("Payloads number to send: %d (%d)\n", 1, strlen(packet->payload));
      packet_queue_remove();
    } else {
      printf("Message NOT sent.\n");
      printf("ERROR: PAYLOADS DISCARD: %d. Cost: %d (%d)\n", 1, get_sink_cost(), strlen(packet->payload));
      return;
    }
  }
  add_comunications(queue_length,queue_length,0,0);
}
/*---------------------------------------------------------------------------*/
static int
payload_queue_number(void){
  int i = 0;
  int queue_length = packet_queue_number();
  int counter = 0;

  /* Count payloads number to send */
  struct packetqueue_member *item = list_head(packet_list);
  for (i = 0; i < queue_length; ++i) {
    counter = counter + count_payloads(item->payload);
    item = item->next;
  }
  return counter;
}
/*---------------------------------------------------------------------------*/
static int
payloads_to_transmit(int number){
  struct packetqueue_member *item;
  int i, result = 0;
  item = list_head(packet_list);
  for (i = 0; i < number; i++){
    result = result + count_payloads(item->payload);
    item = item->next;
  }
  return result;
}
/*---------------------------------------------------------------------------*/
static int
packets_to_transmit(){

  struct packetqueue_member *item;
  int queue_length = packet_queue_number();
  if (AGGREGATION)
  {
    item = list_tail(packet_list);
    if(count_payloads(item->payload) < 6 && queue_length > 1){
      return queue_length - 1;
    }else{
      return queue_length;
    }
  }else{
    return queue_length;
  }
}
/*---------------------------------------------------------------------------*/
int packet_queue_number(){
  int count = 0;
  for(packet = list_head(packet_list); packet != NULL; packet = packet->next)
  {     
    count ++;
  }
  return count;
}
/*---------------------------------------------------------------------------*/
static void packet_queue_allocate(char *payload_new){
    int queue_length = packet_queue_number();
    struct packetqueue_member *item;
    struct record *n;
    int i = 0;
    char* payload[PAYLOAD_MAX_LEN+2];
    
    int payloads_discard = 0;
    if(AGGREGATION){
      //Extract all the payloads of the packet queue to a buffer
      char* token;
      // 
      // ==============================
      // Extract individual payloads 
      // ==============================
      //
      item = list_head(packet_list);
      for (i = 0; i < queue_length; i++)
      {
        payload[1] = item->payload;

        if (count_payloads(payload[1]) > 1){
          token = strtok(payload[1], "| ");
          while (token != NULL)
          {
            n = memb_alloc(&record_memb);
            n->message = malloc(1 + strlen(token));
            strcpy(n->message, token); 
            list_add(record_list,n);
          
            token = strtok(NULL,"| ");
          }
        }else{
          n = memb_alloc(&record_memb);
          n->message = malloc(1 + strlen(payload[1]));
          strcpy(n->message,  payload[1]); 
          list_add(record_list,n);
        }
        item = item->next;
        packet_queue_remove();
      }

      // Save new payloads
      if (count_payloads(payload_new) > 1){
        token = strtok(payload_new, "| ");
        while (token != NULL)
        {
          n = memb_alloc(&record_memb);
          n->message = malloc(1 + strlen(token));
          strcpy(n->message, token); 
          if(is_event(n->message))
            list_insert(record_list,NULL,n);
          else
            list_add(record_list,n);  
          token = strtok(NULL,"| ");
        }
      }else{
        n = memb_alloc(&record_memb);
        n->message = malloc(1 + strlen(payload_new));
        strcpy(n->message,  payload_new); 
        if(is_event(n->message))
          list_insert(record_list,NULL,n);
        else
          list_add(record_list,n);
      }
      
      int total_len = -1;
      int count_packets = 0;
      int payloads_discard_len = 0;
      for (n = list_head(record_list); n != NULL; n = n->next)
      {
        total_len = total_len + 1;
        total_len = total_len + strlen(n->message);
        
        if(total_len > PAYLOAD_MAX_LEN){
          
          count_packets = count_packets + 1;
          total_len = strlen(n->message);
        }
        if(count_packets >= MAX_QUEUEBUF){
          payloads_discard_len = payloads_discard_len + strlen(n->message);
        }
      }  


      char buf[PAYLOAD_MAX_LEN+2] = {0};
      int buf_len = 0;
      void* next;
      if(payloads_discard_len > 0){
        for(n = list_head(record_list);  payloads_discard_len > 0 && next != NULL; n = next){
          next = n->next;
          if(!is_event(n->message)){
            payloads_discard_len =  payloads_discard_len -  strlen(n->message);
            free(n->message);   
            list_remove(record_list,n); 
            memb_free(&record_memb,n);    
            payloads_discard = payloads_discard+1;
          }
        }
        if(payloads_discard_len > 0){
          for(n = list_head(record_list);  payloads_discard_len > 0; n = next){
            next = n->next;
            printf("EVENT DISCARD %s\n", n->message);
            payloads_discard_len =  payloads_discard_len -  strlen(n->message);
            free(n->message);   
            list_remove(record_list,n); 
            memb_free(&record_memb,n);    
            payloads_discard = payloads_discard+1;
          }
        }
        printf("Packet QUEUE is full, discarding payloads before create a new ones\n");
        printf("ERROR -> PAYLOADS DISCARD: %d. Cost: %d\n", payloads_discard, get_sink_cost());
      }

      for (n = list_head(record_list); n != NULL; n = n->next)
      {    
        strcat(buf,n->message);
        buf_len = strlen(buf) + strlen(n->next->message)+1;
          
        if(buf_len >= PAYLOAD_MAX_LEN){
          packet = memb_alloc(&packet_memb);
          packet->payload = malloc(1 + strlen(buf));
          strcpy(packet->payload, buf); 
          linkaddr_copy(&packet->PACKETBUF_ESENDER,&this_node_addr);
          linkaddr_copy(&packet->PACKETBUF_ERECEIVER, &sink_node_addr);
          list_add(packet_list,packet);
          buf[0] = 0;
        }else if (strcmp(n->next->message, "")){
          strcat(buf, "|");
        }        
      }

      packet = memb_alloc(&packet_memb);
      packet->payload = malloc(1 + strlen(buf));
      strcpy(packet->payload, buf); 
      linkaddr_copy(&packet->PACKETBUF_ESENDER,&this_node_addr);
      linkaddr_copy(&packet->PACKETBUF_ERECEIVER, &sink_node_addr);
      list_add(packet_list,packet);
      
      //Clear memory
      for(n = list_head(record_list); n != NULL; n = next)
      {     
        next = n->next;
        free(n->message);   
        list_remove(record_list,n); 
        memb_free(&record_memb,n); 
      }
    }
    else{
      item = list_head(packet_list);
      for (i = 0; i < queue_length; i++)
      {
        payload[1] = item->payload;

        n = memb_alloc(&record_memb);
        n->message = malloc(1 + strlen(payload[1]));
        strcpy(n->message,payload[1]); 
        list_add(record_list,n);
        item = item->next;
        packet_queue_remove();
      }
      n = memb_alloc(&record_memb);
      n->message = malloc(1 + strlen(payload_new));
      strcpy(n->message,  payload_new); 
      list_add(record_list,n);
      
      if(queue_length == MAX_QUEUEBUF){
        printf("Packet QUEUE is full, discarding payloads before create a new ones\n");
        printf("ERROR -> PAYLOADS DISCARD: %d. Cost: %d\n", 1, get_sink_cost());
        payloads_discard = 0;
        for (n = list_head(record_list); n != NULL; n = n->next)
        { 
          if(!is_event(n->message)){
            free(n->message);   
            list_remove(record_list,n); 
            memb_free(&record_memb,n);
            payloads_discard = payloads_discard + 1;    
          }
        }
        if(payloads_discard == 0){
          printf("EVENT DISCARD! %s\n", n->message);
          n = list_head(record_list);
          free(n->message);   
          list_remove(record_list,n); 
          memb_free(&record_memb,n);
        } 
      }
      
      for (n = list_head(record_list); n != NULL; n = n->next)
      {    
        packet = memb_alloc(&packet_memb);
        packet->payload = malloc(1 + strlen(n->message));
        strcpy(packet->payload, n->message); 
        linkaddr_copy(&packet->PACKETBUF_ESENDER,&this_node_addr);
        linkaddr_copy(&packet->PACKETBUF_ERECEIVER, &sink_node_addr);
        list_add(packet_list,packet);
      }

      //Clear memory
      void *next;
      for(n = list_head(record_list); n != NULL; n = next)
      {     
        next = n->next;
        free(n->message);   
        list_remove(record_list,n); 
        memb_free(&record_memb,n); 
      }
    
      packet_queue_show();
    }
} 
/*---------------------------------------------------------------------------*/
static void packet_queue_remove(){
  packet = list_head(packet_list);
  free(packet->payload);   
  list_remove(packet_list,packet); 
  memb_free(&packet_memb,packet);    
} 
/*---------------------------------------------------------------------------*/
/* Shows the packet queue contents of the node */
static void
packet_queue_show(void){

  int queue_length = packet_queue_number();
  if (queue_length == 0) {
    printf("No packets in queue.\n");
    return;
  }
  int i = 1;
  for(packet = list_head(packet_list); packet != NULL; packet = packet->next)
  {     
    printf("Packet #%d | From %d.%d | To %d.%d | Payload: \"%s\"\n", i, packet->PACKETBUF_ESENDER.u8[0], packet->PACKETBUF_ESENDER.u8[1], packet->PACKETBUF_ERECEIVER.u8[0],packet->PACKETBUF_ERECEIVER.u8[1], packet->payload);
    i++;
  }
}
/*---------------------------------------------------------------------------*/
static int
send_msg(void){  
  //ENERGEST_ON(//ENERGEST_TYPE_TX);
  printf("Send message Started. Cost: %d Hops\n",  get_sink_cost());
  int to_tx = packets_to_transmit();
  if (to_tx == 0){
    printf("Nothing to send\n");
    return 0;
  }else{
      printf("Sending %d packets with %d payloads\n",to_tx, payloads_to_transmit(to_tx));
  }
  
  //================================
  //====== Choose destination ======
  //================================
  struct neighbor *n;
  linkaddr_t to = {{ 0, 0 }};
  int hops_aux = 200;
  long double batt_aux = 1000;

  for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)){
    // Check sink addr
    if(linkaddr_cmp(&n->addr, &sink_node_addr)){
      linkaddr_copy(&to, &n->addr);
      hops_aux = -1;
      batt_aux = 1000;
    }else{ 
      if(n->hops <= hops_aux){  
        if(n->hops < hops_aux )
        {
          linkaddr_copy(&to, &n->addr);
          hops_aux = n->hops;
          batt_aux = n->batt;
        }
        if(n->hops == hops_aux)
        {
          if(n->batt > batt_aux){
            linkaddr_copy(&to, &n->addr);
            hops_aux = n->hops;
            batt_aux = n->batt;                
          }
        }
      }
     } 
  }
  //================================  
  //========= SEND PACKETS =========
  //================================
  if (AGGREGATION){
    int i, fail = 0;
    struct packetqueue_member *item;
    for (i = 0; i < to_tx; i++){
      item = list_head(packet_list);
      if (can_transmit(count_payloads(item->payload))){
        if(neighbor_can_receive(batt_aux, count_payloads(item->payload))){
          send_one_packet(item->payload,&to);
          packet_queue_remove();
        }else{
          if(fail == 0)
            printf("ERROR: NO NEIGHBORS WITH BATTERY TO RECV PACKET. COST: %d\n", get_sink_cost());
            //ENERGEST_OFF(//ENERGEST_TYPE_TX);
            fail = 1;
        } 
      }else{
        if(fail == 0)
          printf("ERROR: NO BATTERY TO SEND THE INFORMATION. COST: %d\n", get_sink_cost());
        fail = 1;
      }
    }
  
    if (fail == 1){
      return 0;
    }else{
      return 1;
    }
  
  }
  else{
    if (can_transmit(to_tx)){
      if(neighbor_can_receive(batt_aux, to_tx)){
        send_all_packets(&to); 
        //ENERGEST_OFF(//ENERGEST_TYPE_TX);
        return 1;
      }else{
        printf("ERROR: NO NEIGHBORS WITH BATTERY TO RECV PACKET. COST: %d\n", get_sink_cost());
        //ENERGEST_OFF(//ENERGEST_TYPE_TX);
        return 0;
      }
    }else
    {
      printf("ERROR: NO BATTERY TO SEND THE INFORMATION. COST: %d\n", get_sink_cost());
      return 0;      
    } 
  }
}
/*---------------------------------------------------------------------------*/
/* mesh callback triggered when a packet suffers a timeout */
static void
timedout(struct mesh_conn *c, uint8_t timeout_src){
  printf("Packet timedout!\n");

  if (timeout_src) {
    printf("Route not found, retrying.\n");
    //route_discovery_discover(&(mesh.route_discovery_conn),
    //                         &sink_node_addr, CLOCK_SECOND * 10);
  }
}
/*---------------------------------------------------------------------------*/
/* Transmit state */
static int
transmit(){

  printf("I GO TX WITH COST: %d\n", get_sink_cost());
  // Aqui nao transmite payloads_number!! Transmite apenas os multiplos se tiver AGGR ON
  if(send_msg()){
      printf("TX DONE WITH COST: %d\n", get_sink_cost());
      refresh_broadcast_msg();
      return 1; 
  }else{
    printf(" TX FAIL -> function send_msg\n");
    return 0;
  }
}
/*---------------------------------------------------------------------------*/
/* mesh callback triggered when the node receives a packet */
static void
recv(struct mesh_conn *c, const linkaddr_t *from){

  printf("Data received from %d.%d: %.*s (%d)\n",
         from->u8[0], from->u8[1],
         packetbuf_datalen(), (char *)packetbuf_dataptr(), packetbuf_datalen()-1);
  
  char string[PAYLOAD_MAX_LEN]; 
  sprintf(string,"%s", (char *)packetbuf_dataptr());
  int payload_counter = count_payloads(string);
  printf("Payloads received %d\n", payload_counter);
  
  packet_queue_allocate(string);
  printf("Received packet saved into packet queue. (%d)\n", strlen(string));
  sensor_doing_operation(3);
  if(can_receive(payload_counter)){
    if (AGGREGATION){
      add_comunications(0,0,1,payload_counter);
    }else{
      add_comunications(0,0,payload_counter,payload_counter);
    }
    refresh_broadcast_msg();
  } 
}
/*---------------------------------------------------------------------------*/
/* Shows the current routing table of the node */
/*
static void
route_print_table(void){
  struct route_entry *e = NULL;
  int i = 0;

  printf("Showing all entries from routing table.\n");
  do {
    e = route_get(i);
    if (e == NULL) {
      break;
    }
    printf("Route to %d.%d with nexthop %d.%d and cost %d.\n",
      e->dest.u8[0], e->dest.u8[1],
      e->nexthop.u8[0], e->nexthop.u8[1],
      e->cost);
    ++i;
  } while (1);
}*/
/*---------------------------------------------------------------------------*/
/* Starts the process of discover a route to the Sink Node */
/*
static void
route_start_discovery(void){
  printf("Route Discovery Started.\n");
  route_discovery_discover(&(mesh.route_discovery_conn),
        &sink_node_addr, CLOCK_SECOND * ROUTE_REQUEST);
}
*/
/*---------------------------------------------------------------------------*/
/* Triggers the process of performing a sensing */
float
floor(float x){
  if(x >= 0.0f) {
    return (float)((int)x);
  } else {
    return (float)((int)x - 1);
  }
}
/*---------------------------------------------------------------------------*/
int event()
{
    int i,j, ni;
    printf("simu timeslot %d\n", simu_timeslot);
    for(i = 0; i < sizeof(event_timeslot) / sizeof(event_timeslot[0]); i++)
    {
        for (j = 0; j < event_duration; j++)
        {
          if(event_timeslot[i]+j == simu_timeslot){
            //printf("Event test %d\n", event_timeslot[i]);
            //printf("Prolongado ao %d timeslot\n", j);
            for (ni = 0; ni < 4; ni++)
            {
              if(this_node_addr.u8[0] == event_capture[i][ni]){
                printf ("============ EVENT DETECTED ============\n");
                printf ("Event generated at %d timeslot\n", event_timeslot[i]);
                printf ("Event captured by node %d at timeslot %d with %d hops distance to sink\n", this_node_addr.u8[0], simu_timeslot, get_sink_cost());
                printf ("========================================\n");
                //printf("evento capturado pelo nó %d\n", this_node_addr.u8[0]);
                return 1;
              }
            }
          }
        }
        
    }
    return 0;
}
/*---------------------------------------------------------------------------*/
/* Sense state */
static int
sensor_perform_sensing(void){
  
  /*if(packet_queue_number() >= MAX_QUEUEBUF){
    printf("Packet QUEUE is full, discarding one payload before create a new one\n");
    discard_packets(1);   
  }*/
  unsigned int timestamp = 0;
  char payload[PAYLOAD_MAX_LEN];
   
  printf("Sensors started.\n");
  //ENERGEST_ON(//ENERGEST_TYPE_SENSE);

  #if TIMESYNCH_CONF_ENABLED
    timestamp = timesynch_time();
  #else    
    timestamp = clock_time();
  #endif

  if(event()){
    sprintf(payload, "H:%dE:%uC:%d", 0,timestamp,this_node_addr.u8[0]);
    printf("Event created: %s (%d)\n", payload, strlen(payload));
  }else{
    sprintf(payload, "H:%dT:%uC:%d", 0,timestamp,this_node_addr.u8[0]);
    printf("Payload created: %s (%d)\n", payload, strlen(payload));
  }

  printf("Sensing saved into packet queue. Cost to send: %d Hops\n", get_sink_cost());
  packet_queue_allocate(payload);
  return 1;  
}
/*---------------------------------------------------------------------------*/
static void
operation(int tx_delay, int sx_delay){  
  
  printf("Payloads QUEUE number: %d\n", payload_queue_number());
  
  if(sx_delay != 0){
    // SENSE if have battery
    printf("I GO SENSE WITH COST: %d\n", get_sink_cost());
    if(can_sense()){
      if (sensor_perform_sensing())
      {
        sensor_doing_operation(1);
        printf("SENSE DONE WITH COST: %d\n", get_sink_cost());
        update_battery(1);
      }
    }else{
      printf(" ERROR: NO BATTERY TO PERFORM SENSE. COST: %d\n", get_sink_cost());
    }
  }
  
  // SEND THE INFORMATION if have battery
  if(tx_delay !=0 ){
    transmit();  
  }  
  //sleep state
  sensor_doing_operation(4);
}
//static const struct unicast_callbacks unicast_callbacks = {recv};
const static struct mesh_callbacks callbacks = {recv, sent, timedout};
/*---------------------------------------------------------------------------*/
/* Main Process */
//int sense_simu = 0;
PROCESS_THREAD(sensor_node_process, ev, data){
  
  /* Rime protocol defines node address */
  this_node_addr = linkaddr_node_addr;

  /* Defines manualy sink address */
  sink_node_addr.u8[0] = 1;
  sink_node_addr.u8[1] = 0;
  
  PROCESS_EXITHANDLER(mesh_close(&mesh);)

  double fixed_perc_energy = PER_BATT_INIT;
  unsigned variation = 1;

  PROCESS_BEGIN();
  DBG("[Sensor Node %d] Process begin.\n", this_node_addr.u8[0]);

  mesh_open(&mesh, 132, &callbacks);
  DBG("[Sensor Node %d] Opened mesh Connection.\n", this_node_addr.u8[0]);

  packetqueue_init(&sensor_packet_queue);
  DBG("[Sensor Node %d] Initialized Packet Queue.\n", this_node_addr.u8[0]);

  static struct etimer et, sense_delay_wait, tx_delay_wait;
  
  random_init(this_node_addr.u8[0]*37);
  etimer_set(&et, random_rand() % (SENSE_DELAY_MAX*CLOCK_SECOND));
  
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
  //route_start_discovery();
  
  /* Allow some time for the network to settle. */
  DBG("[Sensor Node %d] Waiting for the network configuration.\n", this_node_addr.u8[0]);
  
  etimer_set(&et, NETWORK_STABILIZE * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  
  DBG("[Sensor Node %d] Started Looking for Sink.\n", this_node_addr.u8[0]);
  powertrace_start(CLOCK_SECOND * POWER_PERIOD, POWER_PERIOD, fixed_perc_energy, variation);
  DBG("[Sensor Node %d] Powertrace started at %d clock ticks.\n", this_node_addr.u8[0], timesynch_time());


  /* Energest module give to us the time that nodes operates in each state*/
  energest_init();
  
  /* Set timers to timslot delay and transmit delay max */
  etimer_set(&sense_delay_wait,SENSE_DELAY_MAX*CLOCK_SECOND);
  etimer_set(&tx_delay_wait,TRANSMIT_DELAY_MAX*CLOCK_SECOND);

  /* aux to transmit delay max */
  int tx_ready = 0;
  int sx_ready = 0;
  while(1) {    
  
    if(AUTO){
      //ENERGEST_ON(//ENERGEST_TYPE_SLEEP);
      
      //Verify if the delay max to transmit and sense has done
      PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&sense_delay_wait));
      sx_ready = etimer_expired(&sense_delay_wait);
      tx_ready = etimer_expired(&tx_delay_wait);
     
      //ENERGEST_OFF(//ENERGEST_TYPE_SLEEP);
      operation(tx_ready, sx_ready);

      // Reset Delays
      if(tx_ready != 0){
        etimer_reset(&tx_delay_wait);
      }
      if(sx_ready != 0){
        simu_timeslot = simu_timeslot + 1;
        etimer_reset(&sense_delay_wait);
      }
    }else{
      PROCESS_YIELD();
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Process that handles serial commands */
PROCESS_THREAD(serial_input, ev, data)
{
 PROCESS_BEGIN();

 for(;;) {
   PROCESS_YIELD();
   if(ev == serial_line_event_message) {
     DBG("Received Serial Input: %s\n", (char *)data);
     if (!strcmp((char *)data, "routes")) {
      //route_print_table();
     } else if (!strcmp((char *)data, "discover")) {
      //route_start_discovery();
     } else if (!strcmp((char *)data, "send")) {
      send_msg();
     } else if (!strcmp((char *)data, "sense")) {
      sensor_perform_sensing();
     }else if(!strcmp((char *)data,"test")){
       printf("test function here!\n");
     } else if (!strcmp((char *)data, "queue")) {
      packet_queue_show();
     }else if (!strcmp((char *)data, "battery")) {
      printf("Battery charge: %lu\n", (unsigned long)get_battery_charge());
     } else if (!strcmp((char *)data, "neighbors")){
      print_neighbors();
     } else {
      printf("Unknown command.\n");
     }
   }
 }
 PROCESS_END();
}
/*---------------------------------------------------------------------------*/