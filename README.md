# README #

This guideline provides necessary instructions for building a sensor platform in the IoT
context, creating a Wireless Sensor Network based on Contiki OS.

The development of the entire model was carried out through the Cooja framework. For the
execution of this guideline, it is necessary to install Linux distribution – Ubuntu.
With this document, a simulation platform bases on all the restriction mentioned during the
dissertation are created.

The goal of the repository https://bitbucket.org/marcosantosilva/mobiwise/ is to create a
functional example that replicates the findings in MobiWise work (Heuristic and Simulation
of Energy Harvesting IoT Nertworks). For that, source code for the Sink Node and Sensor
Node is developed, as well some changes to the underlying network modules are necessary.
The developed model allows the easy adjustment of some network parameters, which can be
configured, considering the network's utility.

###  Warning Note ###

This user manual refers to a beta version of the entire application provided. This means that
there may be future changes. Thus, some features can be added or removed until the final
product.
At the time of launching the final version of this application, this manual will be updated.

### Getting Started ###

1 Contiki official page: http://www.contiki-os.org

2 Some tutorials and examples at http://anrg.usc.edu/contiki/index.php/Contiki_tutorials
and https://senstools.gforge.inria.fr/doku.php?id=contiki%3Aexamples

3 Contiki timers: https://github.com/contiki-os/contiki/wiki/Timers

4 Instruction manual:
https://www.researchgate.net/file.PostFileLoader.html?id=5912cfa3f7b67ef5d630d3bc
&assetKey=AS%3A492384527097856%401494405027722

5 Important exercises: https://team.inria.fr/fun/files/2014/04/exercise_partII.pdf

### Installing Contiki OS and Cooja Simulator ###

1 Go to https://bitbucket.org/marcosantosilva/mobiwise/ and download Contiki with all
the files developed. Unzip and open the folder.

2 Build Cooja with extra memory: cd contiki/tools/cooja && ant run_bigmem. If exists
some git error: git submodule update - -init && ant run

### Create Simulation ###

In the topology of our model, we use two types of sensors. The “sensor_nodes” responsible
for capturing information from the environment and forwarding this information throughout
the entire network. This information is destined for the “sink_node”, where all the data is
stored.

So, to compile the simulation used in this work, we will create a grid topology with 120 nodes
of the type “sensor_nodes” and only one “sink_node”. These values and the topology can be
changed according to personal preferences.

1 Go to File -> Create new simulation.

2 Choose a Simulation Name; Radio Medium: Unit Disk Graph Medium (UDGM):
Distance Loss; Mote startup delay (ms): 1.000; Random Seed: 123.456; New random
seed on reload: No.

3 After everything is configured, several windows are opened: Simulation Window,
Simulation Control Window, Notes, Mote Output and Timeline. Note that these
windows can all be changed. These are just a few examples of the numerous plugins
that Cooja has at its disposal.

### Start Simulation ###

A - Sink node

A1 Go to Mote -> Create New Mote Type -> Sky Mote

A2 Click on Browse and open: Firmware -> contiki/examples/contiki-mobiwise/sinknode.c

A3 Compile and create.

A4 Select number of new motes to 1. Select Random positioning and 0 in all the axis
range. Add motes.

B - Sensor Node

B1 Go to Mote -> Create New Mote Type -> Sky Mote

B2 Click on Browse and open: Firmware -> Contiki/examples/Contiki-mobiwise/sensornode.c

B3 Compile and create.

B4 Select number of new motes to 120. Select Linear positioning. X and y must be
between -250 and 250. Z selects as default.

### Configure Network ###

1 In the Network window: View -> Mote IDs; View -> Mote type; View-> Radio traffic;

Sensor nodes and sink node are differentiated with a different color. If there is an
overlapping sensor, it can be repositioned with the mouse cursor, until reaching the
following topology.

2 To configure motes communication ranges: right button click on mote -> change
transmission ranges.

3 Go to Simulation Control -> Start. All sensor messages are shown in the Mote Output.