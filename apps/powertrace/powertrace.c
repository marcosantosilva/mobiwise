/*
 * Copyright (c) 2019, Centre for Informatics and 
 * Systems of the University of Coimbra.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Powertrace and KIBAM battery
 * \author
 *         Code by Marco Silva <mass@dei.uc.pt>
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "sys/compower.h"
#include "net/rime/rime.h"
#include <math.h>

#include "powertrace.h"
#include "sensor-node.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define DEBUG 1
#if DEBUG
#define DBG(...) printf(__VA_ARGS__);
#else
#define DBG(...)
#endif

//Current Values for harvesting;
/*
 * HARVESTING = 0 -> No Harvesting;
 * HARVESTING = 1 -> High Harvesting; 
 * HARVESTING = 2 -> Medium Harvesting;e
 * HARVESTING = 3 -> Low Harvesting;
*/
#if DYNAMIC_HAVESTING == 0
    #if HARVESTING == 1
      #define solar_charging  1.56
    #elif HARVESTING == 2
      #define solar_charging  0.0229
    #elif HARVESTING == 3
      #define solar_charging  0.0196
    #endif
#endif

PROCESS(powertrace_process, "Periodic power output");

unsigned timer_couter=0;
/*---------------------------------------------------------------------------*/
double total_consumption_sleep=0;
double total_consumption_sense=0;
long total_consumption_tx=0;
long total_consumption_rx=0;
double total_consumption_harvesting=0;
/*---------------------------------------------------------------------------*/
int timeslot_aux = -POWER_PERIOD;
int harvesting_aux = 0;
/*---------------------------------------------------------------------------*/
double periodic_consumption=0.0;
int simu_current_time = -POWER_PERIOD;
/*---------------------------------------------------------------------------*/
struct battery{
  // Maximum amount of charge in battery (J) - static
   double qmax;
  // Available charge at beginning of time step (microAh)
   long double q1_0;
}; struct battery batt; // Parameters of the Battery 
/*---------------------------------------------------------------------------*/
const double vec_solar_charging[60] = {0.152717734878577,	0.165996296719600,	0, 0.115031840499778,	0.231058014556718,	0.265936125259238,	0.108007752102441,	0.383381385095029,
                                   0.371945836800312,	0.411611904807295,	0.461653695900501,	0.502072896775386,	0.693863010134678,	0.789985114185368,	1.12307581067214,
                                   1.10762183477243,	1.23765983874463,	  1.41647969910322,	  1.54321504025195,  	1.92566130012064,	  1.99139520823449,	  2.12050600110209,	
                                   2.36580780157802,  2.42999280469723,	  2.56256227620503,	  2.64558042166513,	  2.73681296535984,   2.97796586874598,  	2.95683368255836,
                                   2.91896188387292,	3.02878063981322,   2.85642858014067,  	2.80642884926382,	  2.57921357842711,	  2.60661477663935,  	2.32127318444512,	
                                   2.20347711778874,  2.18773435606318,	  1.96942358112107,   1.69932737358055,	  1.55971383017672,	  1.43727237709961,	  1.26790575109172,
                                   0.961439051519601,	0.852782877061401,	0.673424743499648,	0.680985281250009,  0.444020973696908,	0.532954393526543,	0.316836087416210,
                                   0.236020810023226,	0.396618479045383,	0.373026283610001,	0.100495980378775,  0.125108309996590,	0.250066714678043,	0.0345384412154741,
                                   0.189820369236071,	0.106478381288377,	0.01845420};
/*---------------------------------------------------------------------------*/
struct comunication{
  int headers_tx;
  int headers_rx;
  int payloads_tx;
  int payloads_rx;
  int do_sense;
  int do_sleep;
  int do_tx;
  int do_rx;
};struct comunication comm;
/*---------------------------------------------------------------------------*/
struct stats{
  unsigned long sense;
  unsigned long lpm;
  unsigned long transmit;
  unsigned long listen;
}; struct stats stats_com;
/*---------------------------------------------------------------------------*/
struct energy_states{
	const double sense; //8Mhz
	const double low_power;
	const double tx;
	const double rx;
};
/*---------------------------------------------------------------------------*/
struct state_consumption{
  const double header_tx;
  const double header_rx;
  const double payload_tx;
  const double payload_rx;
};
/*---------------------------------------------------------------------------*/
// Consumptions for the following states:
// Sensing state | Sleeping State | Transmiting data State | Receiving data State
const struct energy_states i_energyStt = {SENSE_CONSUMPTION, SLEEP_CONSUMPTION, TX_CONSUMPTION, RX_CONSUMPTION};
/*---------------------------------------------------------------------------*/
//Current values for the transmition and reception
// transmiting a header | Receiving Header | transmiting a payload | Receiving a payload 
const struct state_consumption i_comsumptioStt = {TX_HEADER_CONSUMPTION,RX_HEADER_CONSUMPTION,TX_PAYLOAD_CONSUMPTION,TX_PAYLOAD_CONSUMPTION};
/*---------------------------------------------------------------------------*/
void powertrace_print(char *str){
	
  static unsigned long last_sense,last_lpm, last_transmit, last_listen;

  unsigned long sense, lpm, transmit, listen;
  unsigned long all_sense, all_lpm, all_transmit, all_listen;

  static unsigned long seqno;

  energest_flush();
  
  all_sense = energest_type_time(ENERGEST_TYPE_SENSE);
  all_lpm = energest_type_time(ENERGEST_TYPE_SLEEP);
  all_transmit = energest_type_time(ENERGEST_TYPE_TX);
  all_listen = energest_type_time(ENERGEST_TYPE_RX);
  
  sense = all_sense - last_sense;
  lpm = all_lpm - last_lpm;
  transmit = all_transmit - last_transmit;
  listen = all_listen - last_listen;
  
  last_sense = energest_type_time(ENERGEST_TYPE_SENSE);
  last_lpm = energest_type_time(ENERGEST_TYPE_SLEEP);
  last_transmit = energest_type_time(ENERGEST_TYPE_TX);
  last_listen = energest_type_time(ENERGEST_TYPE_RX);
  
  stats_com.sense = sense;
  stats_com.lpm = lpm;
  stats_com.transmit = transmit;
  stats_com.listen = listen;
  
  //radio = transmit + listen;
  //time = sense + lpm + transmit + listen;
  //all_time = all_sense + all_lpm + all_transmit + all_listen;
  //all_radio = energest_type_time(ENERGEST_TYPE_TX) + energest_type_time(ENERGEST_TYPE_RX);

  seqno++;
}
/*---------------------------------------------------------------------------*/
void powertrace_start(clock_time_t period, unsigned seconds, double fixed_perc_energy, unsigned variation){
	double random_variable;
	random_variable = fixed_perc_energy + ((rand() % variation)*0.01);

	DBG("-------------------------------------------\n");
	DBG("KIBAM Battery has been started \n");
	DBG("KIBAM Battery: rand() is %u and Random variable is %u \n",(unsigned) rand()%variation, (unsigned) (random_variable*100));
	
	batt.qmax = BATTERY_MAX;
	batt.q1_0 = BATTERY_MAX * random_variable;

  DBG("KIBAM Battery: qmax = %lu | q1 = %lu \n",(long unsigned) batt.qmax, (long unsigned) batt.q1_0);
	DBG("-------------------------------------------\n");
	
	//btt_interval_sec=seconds;//gives it in seconds
	//btt_interval_min=seconds/60;

  comm.headers_rx = 0;
  comm.headers_tx = 0;
  comm.payloads_rx = 0;
  comm.payloads_tx = 0;
  comm.do_rx = 0;
  comm.do_tx = 0;
  comm.do_sense = 0;
  comm.do_sleep = 0;

  process_start(&powertrace_process, (void *)&period);
}
/*---------------------------------------------------------------------------*/
void powertrace_stop(void){
  process_exit(&powertrace_process);
}
/*---------------------------------------------------------------------------*/
void sensor_doing_operation(int op){
  if(op == 1){
    comm.do_sense = 1;
  }else if(op == 2){ 
    comm.do_tx = 1;
  }else if(op == 3){
    comm.do_rx = 1;
  }else if(op == 4){
    comm.do_sleep = 1;
  }
}
/*---------------------------------------------------------------------------*/
void update_battery(int operation){
  
  DBG("Battery update\n");

  // HARVESTING: Increment a state here
  int num_states=4;
  double current_draw[num_states];
  unsigned long time_each_stt[num_states];
  //int stt_couter;
  int i1, i2;
  double aux;
  // Variables related to Time conversion
  //double convert_tick2sec=0.000030517578125; //A single time tick
  //double convert_sec2hour=0.0002777777777778;// 1/3600
  //unsigned long convert_tick2nano=30517;// a nanoseconds for a single time tick

  //-------- Set the current spend/collect in every state --------
  current_draw[0] = i_energyStt.sense;
  current_draw[1] = i_energyStt.low_power;
  current_draw[2] = i_energyStt.tx;
  current_draw[3] = i_energyStt.rx;
  // HARVESTING: SET THE HARVESTING CURRENT
  current_draw[4] = solar_charging;

  //-------- Set the time spent in every state -----------------
  powertrace_print("");// In this fuction time in state is updated
  //time_each_stt[0] = stats_com.sense;
  time_each_stt[0] = stats_com.sense;
  time_each_stt[1] = stats_com.lpm;
  time_each_stt[2] = stats_com.transmit;
  time_each_stt[3] = stats_com.listen;

  // HARVESTING: SET THE HARVESTING TIME
  time_each_stt[4] = (stats_com.sense+stats_com.lpm+stats_com.listen+stats_com.transmit); 
  
  //-------- call the Kinetic model -----------------
    
	DBG("-------------------SENSE----------------------\n");

  if(comm.do_sense == 1){
    comm.do_sense = 0;
    periodic_consumption = (current_draw[0]);//*(time_each_stt[stt_couter]*convert_tick2sec));
    total_consumption_sense = total_consumption_sense + periodic_consumption;//*(time_each_stt[stt_couter]*convert_tick2sec));
	
    // DEBUG PRINTS
    aux = periodic_consumption/0.001; // Convert to mJ
    i1 = (int) aux;
    i2 = ((aux) - i1) * 100;
    DBG("SENSE Periodic Consumption: %d.%d mJ\n", i1, i2);
    aux = total_consumption_sense / 0.001;
    i1 = (int) aux;
    i2 = ( aux - i1) * 100;
    DBG("SENSE Total Consumption: %d.%d mJ\n", i1, i2);

    i1 = (int) batt.q1_0;
    i2 = (  batt.q1_0 - i1) * 100;
    DBG("BL before state update: %d.%d\n",i1,i2);
    

    batt.q1_0 = batt.q1_0 - periodic_consumption;
    
    if (batt.q1_0 > BATTERY_MAX)
    {
      batt.q1_0 = BATTERY_MAX;
    }else if (batt.q1_0 < 0)
    { 
      batt.q1_0 = 0;
    }
    
    // DEBUG PRINTS
    i1 = (int) batt.q1_0;
    i2 = (batt.q1_0 - i1) * 100;
    DBG("BL after state update: %d.%d\n",i1,i2);
  }else{
    // DEBUG PRINTS
    ////periodic_consumption = 0.0 ;
    DBG("SENSE Periodic Consumption: 0.00 mJ\n");
    i1 = (int) total_consumption_sense/0.001;
    i2 = ( total_consumption_sense - i1) * 100;
    DBG("SENSE Total Consumption: %d.%d mJ\n", i1, i2);
  }

	DBG("-------------------SLEEP------------------------\n");
 
  if(comm.do_sleep == 1){
    comm.do_sleep = 0;
        
    total_consumption_sleep = total_consumption_sleep + current_draw[1];
    periodic_consumption = current_draw[1];
    //periodic_consumption = (current_draw[stt_couter]*(time_each_stt[stt_couter]*convert_tick2sec));

    // DEBUG PRINTS
    aux = periodic_consumption / 0.001;
    i1 = (int) aux;
    i2 = (aux - i1) * 100;
    DBG("SLEEP Periodic Consumption: %d.%d mJ\n", i1, i2);

    aux = total_consumption_sleep / 0.001;
    i1 = (int) aux;
    i2 = (aux - i1) * 100;
    DBG("SLEEP Total Consuption: %d.%d mJ\n", i1, i2);

    i1 = (int) batt.q1_0;
    i2 = (  batt.q1_0 - i1) * 100;
    DBG("BL before state update: %d.%d\n",i1,i2);
    //============================================

    batt.q1_0 = batt.q1_0 - periodic_consumption;
    if (batt.q1_0 > BATTERY_MAX)
    {
      batt.q1_0 = BATTERY_MAX;
    }else if (batt.q1_0 < 0)
    { 
      batt.q1_0 = 0;
    }
    
    // DEBUG PRINTS
    i1 = (int) batt.q1_0;
    i2 = (batt.q1_0 - i1) * 100;
    DBG("BL after state update: %d.%d\n",i1,i2);
  }else{
    // DEBUG PRINTS
    DBG("SLEEP Periodic Consumption: 0.00 mJ\n");
    aux = total_consumption_sleep / 0.001;
    i1 = (int) aux;
    i2 = ( aux - i1) * 100;
    DBG("SLEEP Total Consumption: %d.%d mJ\n", i1, i2);
  }

  DBG("-----------------TX--------------------------\n"); 
  
  if( comm.do_tx == 1){
    if (AGGREGATION){
      comm.do_tx = 0;

      total_consumption_tx=total_consumption_tx + (current_draw[2] +  comm.headers_tx*i_comsumptioStt.header_tx + comm.payloads_tx*i_comsumptioStt.payload_tx);
      periodic_consumption =  current_draw[2] + (comm.headers_tx*i_comsumptioStt.header_tx +  comm.payloads_tx*i_comsumptioStt.payload_tx);
  
      // DEBUG PRINTS// DEBUG PRINTS
      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("TX Periodic Consumption: %d.%d J\n", i1, i2);
    
      i1 = (int)  total_consumption_tx;
      i2 = ( total_consumption_tx - i1) * 100;
      DBG("TX Total Consumption: %d.%d J\n", i1, i2);
      comm.payloads_tx = 0;
      comm.headers_tx = 0;
    
      i1 = (int) batt.q1_0;
      i2 = (  batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //===============================================
      batt.q1_0 = batt.q1_0 - periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
    }else{
      comm.do_tx = 0;

      total_consumption_tx= total_consumption_tx + ( current_draw[2]*comm.payloads_tx +  comm.headers_tx*i_comsumptioStt.header_tx + comm.payloads_tx*i_comsumptioStt.payload_tx);
      periodic_consumption =  current_draw[2]*comm.payloads_tx + ( comm.headers_tx*i_comsumptioStt.header_tx +  comm.payloads_tx*i_comsumptioStt.payload_tx);
      // DEBUG PRINTS
      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("TX Periodic Consumption: %d.%d J\n", i1, i2);
    
      i1 = (int)  total_consumption_tx;
      i2 = ( total_consumption_tx - i1) * 100;
      DBG("TX Total Consumption: %d.%d J\n", i1, i2);
      comm.payloads_tx = 0;
      comm.headers_tx = 0;
    
      i1 = (int) batt.q1_0;
      i2 = (  batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //==============================================
      batt.q1_0 = batt.q1_0 - periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
    }    
  }else{
    // DEBUG PRINTS
    DBG("TX Periodic Consumption: 0.00 J\n");
    i1 = (int)  total_consumption_tx;
    i2 = ( total_consumption_tx - i1) * 100;
    DBG("TX Total Consumption: %d.%d J\n", i1, i2);
  }  

  DBG("--------------------RX-----------------------\n");

  if(comm.do_rx == 1){
    if (AGGREGATION){
      comm.do_rx = 0;

      total_consumption_rx = total_consumption_rx + ((current_draw[3]) + comm.headers_rx*i_comsumptioStt.header_rx + comm.payloads_rx*i_comsumptioStt.payload_rx);
      periodic_consumption = ((current_draw[3]) + comm.headers_rx*i_comsumptioStt.header_rx + comm.payloads_rx*i_comsumptioStt.payload_rx);
      // DEBUG PRINTS
      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("RX Periodic Consumption: %d.%d J\n", i1, i2);
      i1 = (int)  total_consumption_rx;
      i2 = ( total_consumption_rx - i1) * 100;
      DBG("RX Total Consumption: %d.%d J\n", i1, i2);
      comm.payloads_rx = 0;
      comm.headers_rx = 0;

      i1 = (int) batt.q1_0;
      i2 = (  batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //===========================================
      batt.q1_0 = batt.q1_0 - periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
    }else{
      comm.do_rx = 0;
      
      total_consumption_rx = total_consumption_rx + ((current_draw[3]*comm.payloads_rx) + comm.headers_rx*i_comsumptioStt.header_rx + comm.payloads_rx*i_comsumptioStt.payload_rx);
      periodic_consumption = ((current_draw[3]*comm.payloads_rx) + comm.headers_rx*i_comsumptioStt.header_rx + comm.payloads_rx*i_comsumptioStt.payload_rx);
      // DEBUG PRINTS
      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("RX Periodic Consumption: %d.%d J\n", i1, i2);
      i1 = (int)  total_consumption_rx;
      i2 = ( total_consumption_rx - i1) * 100;
      DBG("RX Total Consumption: %d.%d J\n", i1, i2);
      comm.payloads_rx = 0;
      comm.headers_rx = 0;

      i1 = (int) batt.q1_0;
      i2 = (  batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //===================================================
      batt.q1_0 = batt.q1_0 - periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
    }
  }else{
    // DEBUG PRINTS
    periodic_consumption = 0.0;
    DBG("RX Periodic Consumption: 0.00 J\n");
    i1 = (int)  total_consumption_rx;
    i2 = ( total_consumption_rx - i1) * 100;
    DBG("RX Total Consumption: %d.%d J\n", i1, i2);
  }
  
  if(operation == 0){
	  DBG("-----------------HARVESTING--------------------------\n");

    if(DYNAMIC_HARVESTING == 1){
      timeslot_aux = timeslot_aux + POWER_PERIOD;

      if(timeslot_aux >= 60){ // COLOCAR 300 ********
        timeslot_aux = 0;
        harvesting_aux =  harvesting_aux + 1;
        if(harvesting_aux > 59){
          harvesting_aux = 0;
          DBG("Day/Night cicle - Complete\n");
        }  
      }

      i1 = (int) (vec_solar_charging[harvesting_aux]);
      i2 = ((vec_solar_charging[harvesting_aux]) - i1) * 100;
     
      //total_consumption_harvesting =total_consumption_harvesting + (-((vec_solar_charging[harvesting_aux])/12)*time_each_stt[stt_couter]*convert_tick2sec);
      total_consumption_harvesting =total_consumption_harvesting + (-vec_solar_charging[harvesting_aux]);
      //periodic_consumption = (-((vec_solar_charging[harvesting_aux])/12)*time_each_stt[stt_couter]*convert_tick2sec);
      periodic_consumption = -(vec_solar_charging[harvesting_aux]);
    
      // DEBUG PRINTS
      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("HARVESTING Periodic Consumption: %d.%d J\n", -i1, -i2);
      i1 = (int) total_consumption_harvesting;
      i2 = ( total_consumption_harvesting - i1) * 100;
      DBG("HARVESTING Total Consumption: %d.%d J\n", -i1, -i2);

      i1 = (int) batt.q1_0;
      i2 = (  batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //================================================  
      batt.q1_0 = batt.q1_0 - periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
        
    }else{
      total_consumption_harvesting =total_consumption_harvesting + get_current_harvesting();
      periodic_consumption = get_current_harvesting();
      // DEBUG PRINTS
      i1 = (int) (get_current_harvesting());
      i2 = ((get_current_harvesting()) - i1) * 100;
      DBG("Valor do harvesting: %d.%d\n",i1,i2);

      i1 = (int) periodic_consumption;
      i2 = (periodic_consumption - i1) * 100;
      DBG("HARVESTING Periodic Consumption: %d.%d\n", i1, i2);
      i1 = (int) total_consumption_harvesting;
      i2 = ( total_consumption_harvesting - i1) * 100;
      DBG("HARVESTING Total Consumption: %d.%d\n", i1, i2);
    
      i1 = (int) batt.q1_0;
      i2 = ( batt.q1_0 - i1) * 100;
      DBG("BL before state update: %d.%d\n",i1,i2);
      //=================================================
      batt.q1_0 = batt.q1_0 + periodic_consumption;
      if (batt.q1_0 > BATTERY_MAX)
      {
        batt.q1_0 = BATTERY_MAX;
      }else if (batt.q1_0 < 0)
      { 
        batt.q1_0 = 0;
      }
      // DEBUG PRINTS
      i1 = (int) batt.q1_0;
      i2 = (batt.q1_0 - i1) * 100;
      DBG("BL after state update: %d.%d\n",i1,i2);
    }  
  }    

  // prints 
	//DBG("KIBAM Battery: Energy computation for state %d \n",stt_couter);
	//DBG("KIBAM Battery: Available charge at next time interval is %lu (microAh) \n",(unsigned long) (batt.q1_0));
	//DBG("Battery: Bound charge at next time interval is %lu (microAh) \n",(unsigned long) (batt.q2_0));
	//DBG("KIBAM Battery: Time in state %d in nanoseconds is %lu \n",stt_couter,(unsigned long)time_each_stt[stt_couter]*convert_tick2sec);
  //DBG("Battery: Seconds in state %d is %4lu \n",stt_couter,to_seconds(time_each_stt[stt_couter]));
	//DBG("Battery: Number ticks in state %d is %lu \n",stt_couter,time_each_stt[stt_couter]);
	DBG("-------------------------------------------\n");  
  // Print
  
	//DBG("General Stats of the battery\n");
	//DBG("KIBAM Battery: Residual (microAh); %lu; \n",(unsigned long) (batt.q1_0));
	//DBG("KIBAM Battery: Bound charge at next time interval is %lu (microAh) \n",(unsigned long) (batt.q2_0));
	//DBG("KIBAM Battery: Energy Total consumption (microA); %lu;  \n",(unsigned long) (total_consumption));
	//DBG("KIBAM Battery: Energy Periodic consumption (microA); %d;  \n",(int) (periodic_consumption));
	//DBG("Energy: CPU ticks %lu, LPM %lu, Tx %lu, Rx %lu \n", stats_com.cpu, stats_com.lpm, stats_com.transmit, stats_com.listen);
	//DBG("-------------------------------------------\n"); 

}
/*---------------------------------------------------------------------------*/
void add_comunications(int txh, int txp, int rxh, int rxp){
  comm.headers_rx = comm.headers_rx + rxh;
  comm.headers_tx = comm.headers_tx + txh;
  comm.payloads_rx = comm.payloads_rx + rxp;
  comm.payloads_tx = comm.payloads_tx + txp;
  update_battery(1);
}
/*---------------------------------------------------------------------------*/
double get_current_harvesting(){
  if(DYNAMIC_HARVESTING == 1){
    return (vec_solar_charging[harvesting_aux]);  
  }else{
    return solar_charging;
  }
}
/*---------------------------------------------------------------------------*/
double get_simu_harvesting(){
  
  int simu_time_left = SIMU_TIME - simu_current_time; //seconds
  //int i1 = 0;
  //int i2 = 0;
  if(DYNAMIC_HARVESTING == 1){

    int aux_time = timeslot_aux;
    int aux_harvesting = harvesting_aux;
    double sum_harvesting = 0;
    
    while (simu_time_left > 0)
    {
      if(aux_time >= 60){ 
        aux_time = 0;
        aux_harvesting =  aux_harvesting + 1;
        if(aux_harvesting > 59) aux_harvesting = 0;
      }
    
      sum_harvesting = sum_harvesting + (vec_solar_charging[aux_harvesting]);      
      simu_time_left = simu_time_left - POWER_PERIOD;
      aux_time = aux_time + POWER_PERIOD;
    }
    return (sum_harvesting);  
  }else{
    return((SENSE_DELAY_MAX/POWER_PERIOD)*solar_charging)*(simu_time_left/SENSE_DELAY_MAX);
  }
}
/*---------------------------------------------------------------------------*/
// Verify if a node can transmit
int can_transmit(int p){
 
  if(DYNAMIC_HARVESTING == 1){
    
    if (AGGREGATION){
      if(((get_battery_charge() + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (TX_CONSUMPTION + TX_HEADER_CONSUMPTION + p*TX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (TX_CONSUMPTION + TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((get_battery_charge() + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (TX_CONSUMPTION*p + p*TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (TX_CONSUMPTION*p + p*TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    } 

  }else{

    if (AGGREGATION){
      if(((get_battery_charge() + get_current_harvesting() - (TX_CONSUMPTION + TX_HEADER_CONSUMPTION + p*TX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (TX_CONSUMPTION + TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((get_battery_charge() + get_current_harvesting() - (TX_CONSUMPTION*p + p*TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (TX_CONSUMPTION*p + p*TX_HEADER_CONSUMPTION+ p*TX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    } 
  }

}
/*---------------------------------------------------------------------------*/
// Verify if a node can recv
int can_receive(int p){

  // RECEIVE THE INFORMATION
  if(DYNAMIC_HARVESTING == 1){

    if (AGGREGATION){
      if(((get_battery_charge() + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((get_battery_charge() + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (RX_CONSUMPTION*p + p*RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (RX_CONSUMPTION*p + p*RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }
    
  }else{
    
    if (AGGREGATION){
      if(((get_battery_charge() + get_current_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((get_battery_charge() + get_current_harvesting() - (RX_CONSUMPTION*p + p*RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - (RX_CONSUMPTION*p + p*RX_HEADER_CONSUMPTION + p*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }
  }

}
/*---------------------------------------------------------------------------*/
// Verify if a node can sense
int can_sense(){
  
  if(DYNAMIC_HARVESTING == 1){
    if(((get_battery_charge() + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - SENSE_CONSUMPTION) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - SENSE_CONSUMPTION) >= (0.505*BATTERY_MAX))){
        return 1;
    }else{
        return 0;
    }
  }else{
    if(((get_battery_charge() + (get_current_harvesting()) - SENSE_CONSUMPTION) > (0.1*BATTERY_MAX))
        && ((get_battery_charge() + get_simu_harvesting() - SENSE_CONSUMPTION) >= (0.505*BATTERY_MAX))){
      return 1;
    }else{
      return 0;
    }
  }

}
/*---------------------------------------------------------------------------*/
// Verify if a neighbor can receive
int neighbor_can_receive(long double neighbotBatt, int payloadsNumber){

  // RECEIVE THE INFORMATION
  if(DYNAMIC_HARVESTING == 1){

    if (AGGREGATION){
      if(((neighbotBatt + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((neighbotBatt + get_simu_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((neighbotBatt + ((get_current_harvesting()/60)*SENSE_DELAY_MAX) - (RX_CONSUMPTION*payloadsNumber + payloadsNumber*RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((neighbotBatt + get_simu_harvesting() - (RX_CONSUMPTION*payloadsNumber + payloadsNumber*RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }

    
  }
  else{

    if (AGGREGATION){
      if(((neighbotBatt + get_current_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((neighbotBatt + get_simu_harvesting() - (RX_CONSUMPTION + RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }else{
      if(((neighbotBatt + get_current_harvesting() - (RX_CONSUMPTION*payloadsNumber + payloadsNumber*RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) > (0.1*BATTERY_MAX))
        && ((neighbotBatt + get_simu_harvesting() - (RX_CONSUMPTION*payloadsNumber + payloadsNumber*RX_HEADER_CONSUMPTION + payloadsNumber*RX_PAYLOAD_CONSUMPTION)) >= (0.505*BATTERY_MAX))){
        return 1;
      }else{
        return 0;
      }
    }

  }
}  
/*---------------------------------------------------------------------------*/    
long double get_battery_charge(){
    return batt.q1_0;	
}
/*---------------------------------------------------------------------------*/
double get_max_charge(){
    return batt.qmax;	
}
/*---------------------------------------------------------------------------*/
unsigned long
to_seconds(uint64_t time){
  return (unsigned long)(time / ENERGEST_SECOND);
}
/*---------------------------------------------------------------------------*/
void print_time_states(){
  DBG("\nEnergest sink cost %d:\n", get_sink_cost());
  DBG(" E-SENSE          %4lus E-SLEEP      %4lus \n",
           (to_seconds(energest_type_time(ENERGEST_TYPE_SENSE))),
           (to_seconds(energest_type_time(ENERGEST_TYPE_SLEEP))));
  DBG(" E-Radio LISTEN %4lus  E-TRANSMIT %4lus\n",
           to_seconds(energest_type_time(ENERGEST_TYPE_RX)),
           to_seconds(energest_type_time(ENERGEST_TYPE_TX)));
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(powertrace_process, ev, data){
  static struct etimer periodic;
  clock_time_t *period;
  PROCESS_BEGIN();

  period = data;

  if(period == NULL) {
    PROCESS_EXIT();
  }
  etimer_set(&periodic, *period);

  while(1) {
    PROCESS_WAIT_UNTIL(etimer_expired(&periodic));
    simu_current_time = simu_current_time + POWER_PERIOD;
    if(simu_current_time >= SIMU_TIME){
      simu_current_time = 0;
    }
    etimer_reset(&periodic);

    update_battery(0);
    //refresh_broadcast_msg();
    long double batt = get_battery_charge();
    int i1 = (int) batt;
    int i2 = (batt - i1) * 100;
    printf("Battery level %d.%dJ | Sink Distance: %d \n",i1,i2, get_sink_cost());
    //print_time_states();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/