/*
 * Copyright (c) 2019, Centre for Informatics and 
 * Systems of the University of Coimbra.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Powertrace and KIBAM battery
 * \author
 *         Code by Marco Silva <mass@dei.uc.pt>
 */

#ifndef POWERTRACE_H
#define POWERTRACE_H

#include "sys/clock.h"

#define BATTERY_MAX 648 //Joules
#define PER_BATT_INIT   0.5 //Battery Start - Percentage (0.5 = 50%BATTERY_MAX)
#define ENERGEST_SECOND RTIMER_ARCH_SECOND

/*
 * DYNAMIC_HARVESTING 0 means tha harvesting are a contanst
 * DYNAMIC_HARVESTING 1 means that we are use a dynamic harvesting that simulates the day/night cicle
*/
#define DYNAMIC_HARVESTING 1

/*
 * HARVESTING = 0 -> No Harvesting;
 * HARVESTING = 1 -> High Harvesting; 
 * HARVESTING = 2 -> Medium Harvesting;
 * HARVESTING = 3 -> Low Harvesting;
*/
#define HARVESTING 1

// Energy Consumption per state
// Joules unit
#define SENSE_CONSUMPTION 0.00767

#define SLEEP_CONSUMPTION 0.00063

#define TX_CONSUMPTION 7.63
#define TX_PAYLOAD_CONSUMPTION 0.00946
#define TX_HEADER_CONSUMPTION 0.21796

#define RX_CONSUMPTION 7.63
#define RX_PAYLOAD_CONSUMPTION 0.00628
#define RX_HEADER_CONSUMPTION 0.37585

void powertrace_start(clock_time_t period, unsigned seconds, double fixed_perc_energy, unsigned variation);
void powertrace_stop(void);
void update_battery(int);

void sensor_doing_operation(int);
void add_comunications(int, int, int, int);

typedef enum {
  POWERTRACE_ON,
  POWERTRACE_OFF
} powertrace_onoff_t;

void powertrace_sniff(powertrace_onoff_t onoff);

// Print time in each state
void  print_time_states();

// Refresh time in each state
void powertrace_print(char *str);

// Convert ticks to seconds
unsigned long to_seconds(uint64_t time);

// Get the harvesting that corresponds to the current time slot
double get_current_harvesting();
// Get the harvesting that a node can receive at the end of the simulation
double get_simu_harvesting();
// Get battery charge
long double get_battery_charge(void);

// Verify if node can transmit
int can_transmit(int);
// Verify if a node can recv
int can_receive(int);
// Verify if a node can sense
int can_sense();
// Verify if a neighboor can receive the packet;
int neighbor_can_receive(long double neighbotBatt, int payloadsNumber);

#endif /* POWERTRACE_H */